# ruby-knights-tour

Created with Ruby as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/ruby-programming/lessons/data-structures-and-algorithms). 

# Final Thoughts

Completing this project allowed me to practice with Data Structures, Search Algorithms and Object Oriented Programming in Ruby.
