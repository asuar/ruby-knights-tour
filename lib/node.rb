# frozen_string_literal: true

class Node
  attr_accessor :value, :parent_node, :children_nodes

  def initialize
    @value = nil
    @parent_node = nil
    @children_nodes = []
  end
  
end
