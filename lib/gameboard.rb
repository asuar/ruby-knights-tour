# frozen_string_literal: true

class GameBoard
  require_relative 'tree.rb'
  require_relative 'knight.rb'

  @@WIDTH = 8
  @@HEIGHT = 8

  def initialize
    @knight = Knight.new
    @tree = Tree.new
  end

  public

  def knight_moves(starting_position, ending_position)
    return nil unless valid_move?(starting_position)

    starting_node = @tree.insert(nil, starting_position)
    add_possible_moves(starting_node)
    result_node = breadth_first_search(ending_position)
    print_result(result_node)
  end

  private

  def breadth_first_search(value)
    return @tree.root if @tree.root.value == value

    node_list = []
    current_node = @tree.root
    until current_node.value == value
      current_node.children_nodes.each { |node| node_list << node unless node.nil? }
      # once we check all the children of a node we need add more nodes
      current_node = node_list.shift
      add_possible_moves(current_node)
      return nil if current_node.nil?
    end
    current_node
  end

  def valid_move?(position)
    position[0] < @@WIDTH && position[1] < @@HEIGHT && position[0] > -1 && position[1] > -1
  end

  def add_possible_moves(root)
    # get all possible moves
    possible_moves = @knight.possible_moves(root.value)
    # make sure moves are valid
    valid_moves = possible_moves.select { |position| valid_move?(position) }
    # put them in tree structure
    valid_moves.each { |position| @tree.insert(root, position) }
  end

  def print_result(result_node)
    result = @tree.path_to_root(result_node)
    puts "You made it in #{result.length - 1} moves! Heres your path:"
    result.each do |p|
      print '['
      print p.join(',')
      puts ']'
    end
  end
end
