# frozen_string_literal: true

class Knight
  def initialize; end

  public

  def possible_moves(starting_position)
    # return all possible moves
    # the board should check if a move is valid
    # example [0,0]
    # results [[1,2],[2,1],[2,-1],[1,-2],[-1,-2],[-2,-1],[-2,1],[-1,2]]
    results = []
    results << [starting_position[0] + 1, starting_position[1] + 2]
    results << [starting_position[0] + 2, starting_position[1] + 1]
    results << [starting_position[0] + 2, starting_position[1] - 1]
    results << [starting_position[0] + 1, starting_position[1] - 2]
    results << [starting_position[0] - 1, starting_position[1] - 2]
    results << [starting_position[0] - 2, starting_position[1] - 1]
    results << [starting_position[0] - 2, starting_position[1] + 1]
    results << [starting_position[0] - 1, starting_position[1] + 2]
    results
  end
end
