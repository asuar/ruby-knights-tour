# frozen_string_literal: true

class Tree
  require_relative 'node.rb'

  attr_reader :root

  def initialize
    @root = nil
  end

  public

  def build_tree(values)
    values.each do |value|
      insert(@root, value)
    end
  end

  def inorder(root)
    print '['
    print root.value.each { |p| p }.join(',')
    puts ']'
    root.children_nodes.each { |node| inorder(node) unless node.nil? }
  end

  def insert(root, value)
    node = Node.new
    node.value = value

    if @root.nil?
      @root = node
    elsif root.nil?
      return nil
    else
      root.children_nodes.push(node)
      node.parent_node = root
    end
    node
  end

  def path_to_root(node)
    result = []
    current_node = node
    until current_node.parent_node.nil?
      result.unshift(current_node.value)
      current_node = current_node.parent_node
    end
    result.unshift(current_node.value)
  end

end
