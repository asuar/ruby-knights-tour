# frozen_string_literal: true

require_relative 'gameboard.rb'
require_relative 'knight.rb'

board = GameBoard.new
board.knight_moves([3, 3], [4, 3])
